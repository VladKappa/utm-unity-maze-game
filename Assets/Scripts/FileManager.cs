using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileManager
{
    /// <summary>
    /// Write data to JSON file
    /// </summary>
    /// <param name="fileName">The name of the file</param>
    /// <param name="fileContent">The content that will be write to the file</param>
    public void WriteFile(string fileName, string fileContent)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, fileName);

        try
        {
            File.WriteAllText(fullPath, fileContent);
        }
        catch(Exception ex)
        {
            Debug.LogError($"Failed to write to {fullPath}. Exception message: {ex.Message} {(ex.InnerException?.Message)}");
        }
    }

    /// <summary>
    /// Load data from JSON file
    /// </summary>
    /// <param name="fileName">The name of the file</param>
    /// <param name="result">The result after reading data from file</param>
    public void LoadFile(string fileName, out string result)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, fileName);

        try
        {
            result = File.ReadAllText(fullPath);
        }
        catch
        {
            result = "";
        }
    }
}
