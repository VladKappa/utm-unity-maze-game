using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StareCelula
{
    Neparcurs,
    Curent,
    Parcurs,
    DrumNecesar
}

public class CelulaLabirint : MonoBehaviour
{
    [SerializeField] GameObject[] pereti;
    [SerializeField] MeshRenderer podea;
    [SerializeField] Texture _texturaPerete;
    [SerializeField] Texture _texturaPodeaDefault;
    [SerializeField] Texture _texturaPodeaStart;
    [SerializeField] Texture _texturaPodeaEnd;

    StareCelula _stare;

    public void SeteazaStare(StareCelula stare)
    {
        switch (stare)
        {
            case StareCelula.Neparcurs:
                podea.material.color = Color.red;
                break;
            case StareCelula.Curent:
                podea.material.color = Color.yellow;
                break;
            case StareCelula.Parcurs:
                podea.material.color = Color.blue;
                break;
            case StareCelula.DrumNecesar:
                podea.material.color = Color.grey;
                break;
        }
        _stare = stare;
    }

    public StareCelula getStare()
    {
        return this._stare;
    }

    public MeshRenderer getPodea()
    {
        return this.podea;
    }

		// Aceasta functie ne ajuta sa setam o pozitie putin mai diferita intre pereti
		// care va fi invizibila pentru jucator dar care va rezolva problema texturilor care
		// se suprapun
		public void AjusteazaPereti()
		{
			for(int i=0;i<4;i++)
			{
				Vector3 pozitiaCurenta = pereti[i].gameObject.transform.position;
				if(i>1)
				{
					pozitiaCurenta.z += Random.Range(1f,100f)*0.00001f;
					pereti[i].gameObject.transform.position = pozitiaCurenta;
					continue;
				}
				pozitiaCurenta.x += Random.Range(1f,100f)*0.00001f;
				pereti[i].gameObject.transform.position = pozitiaCurenta;
				//Debug.Log(pozitiaCurenta);
			}
		}

    public void StergePerete(int perete)
    {
            // O celula are urmatorii pereti setati:
            // 0 - +X
            // 1 - -X
            // 2 - +Z
            // 3 - -Z
            pereti[perete].gameObject.SetActive(false);
    }

    public void SetTextures(string celulaType = "default")
    {
        foreach(GameObject perete in pereti)
        {
            perete.GetComponent<Renderer>().material.mainTexture = _texturaPerete;
        }
        podea.material.color = Color.white;
        if (celulaType.ToLower() == "default")
            podea.material.mainTexture = _texturaPodeaDefault;
        else if (celulaType.ToLower() == "start")
        {
            podea.material.mainTexture = _texturaPodeaStart;
            podea.transform.Rotate(0, 90f, 0);
        }
        else if (celulaType.ToLower() == "end")
        {
            podea.material.mainTexture = _texturaPodeaEnd;
            podea.transform.Rotate(0, 90f, 0);
        }
    }

    public bool IsPlayerOn()
    {
        return this.podea.GetComponent<SetCurrentCelula>().IsPlayerOn();
    }
}
