using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class TouchCoin : MonoBehaviour, ISaveable
{
    TMP_Text _timerText;

    private void Start()
    {
        _timerText = FindObjectOfType<TMP_Text>();
    }

    void OnCollisionEnter(Collision other)
    {
        SaveDataConfig sd = new SaveDataConfig();
        LoadJsonData(sd);
        SaveJsonData(this, sd);

        SceneManager.LoadScene("EndScene");
    }

    private void LoadJsonData(SaveDataConfig sd)
    {
        _fileManager.LoadFile("PlayerConfig.dat", out string json);
        if (json != "")
        {
            sd.LoadFromJson(json);
        }
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {

    }

    FileManager _fileManager = new FileManager();

    private void SaveJsonData(TouchCoin touchCoin, SaveDataConfig sd)
    {
        touchCoin.PopulateSaveData(sd);

        _fileManager.WriteFile("PlayerConfig.dat", sd.ToJson());
    }

    public void PopulateSaveData(SaveDataAbstract saveData)
    {
        SaveDataConfig saveConfig = saveData as SaveDataConfig;
        saveConfig._time = _timerText.text;
    }
}
