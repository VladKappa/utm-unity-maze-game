using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour, ISaveable
{
    [Header("Movement")]
    public float moveSpeed;

    public float groundDrag;

    [HideInInspector] public float walkSpeed;
    [HideInInspector] public float sprintSpeed;

    [Header("Start Timer")]
    public GameObject startCamera;
    MoveCamera _startCameraScript;

    [Header("Start Position")]
    public GameObject labirinth;
    GeneratorLabirint _labirinthScript;

    [Header("Keybinds")]
    KeyCode _frontKey, _backKey, _leftKey, _righKey;

    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    bool grounded;

    public Transform orientation;

    float horizontalInput;
    float verticalInput;

    Vector3 moveDirection;

    Rigidbody rb;

    private void Start()
    {
        LoadJsonData(this);

        SetStartPosition();

        _startCameraScript = startCamera.GetComponent<MoveCamera>();

        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }

    private void SetStartPosition()
    {
        _labirinthScript = labirinth.GetComponent<GeneratorLabirint>();
        this.transform.position = _labirinthScript.GetStartPosition();
    }

    private bool MovePermision()
    {
        return _startCameraScript.GetMovePermision();
    }

    private void Update()
    {
        // ground check
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.3f, whatIsGround);

        MyInput();
        SpeedControl();

        // handle drag
        if (grounded)
            rb.drag = groundDrag;
        else
            rb.drag = 0;
    }

    private void FixedUpdate()
    {
        if (MovePermision()) 
            MovePlayer();
    }

    private void MyInput()
    {
        horizontalInput = Input.GetKey(_righKey) ? 1 : Input.GetKey(_leftKey) ? -1 : 0;
        verticalInput = Input.GetKey(_frontKey) ? 1 : Input.GetKey(_backKey) ? -1 : 0;
    }

    private void MovePlayer()
    {
        // calculate movement direction
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;

        // on ground
        if(grounded)
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force);
    }

    private void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        // limit velocity if needed
        if(flatVel.magnitude > moveSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * moveSpeed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
    }

    public void PopulateSaveData(SaveDataAbstract saveData)
    {

    }

    FileManager _fileManager = new FileManager();
    private void LoadJsonData(PlayerMovement playerMove)
    {
        _fileManager.LoadFile("PlayerSettings.dat", out string json);
        if (json != "")
        {
            SaveDataSettings sd = new SaveDataSettings();
            sd.LoadFromJson(json);

            playerMove.LoadFromSaveData(sd);
        }
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {
        SaveDataSettings saveSettings = saveData as SaveDataSettings;
        _leftKey = saveSettings._moveLeft;
        _righKey = saveSettings._moveRight;
        _frontKey = saveSettings._moveFront;
        _backKey = saveSettings._moveBack;
    }
}