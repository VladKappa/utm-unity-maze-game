using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelTimer : MonoBehaviour
{
    [Header("Timer Elements")]
    public TextMeshProUGUI timerText;

    [Header("Cool Down Elements")]
    public GameObject startCamera;
    MoveCamera _startCameraScript;

    private float currentTime;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0;
        _startCameraScript = startCamera.GetComponent<MoveCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CanUpdate())
        {
            currentTime += Time.deltaTime;
            UpdateText(true);
        }
        else
        {
            UpdateText(false);
        }
    }

    bool CanUpdate()
    {
        return _startCameraScript.GetMovePermision();
    }

    void UpdateText(bool update)
    {
        timerText.text = update ? $"{(int)currentTime / 60}:{(currentTime % 60).ToString("f2")}" : "";
    }
}
