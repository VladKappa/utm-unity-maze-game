using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// https://www.youtube.com/watch?v=f473C43s8nE - reference

public class PlayerCam : MonoBehaviour
{

	public float sensX;
	public float sensY;

	public Transform orientation;

    [Header("Start Timer")]
    public GameObject startCamera;
    MoveCamera _startCameraScript;

    float xRotation;
	float yRotation;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;

        _startCameraScript = startCamera.GetComponent<MoveCamera>();
    }
    private bool MovePermision()
    {
        return _startCameraScript.GetMovePermision();
    }

    void Update()
	{
        if (MovePermision())
        {
            float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensX;
            float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensY;

            yRotation += mouseX;
            xRotation -= mouseY;

            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
            orientation.rotation = Quaternion.Euler(0, yRotation, 0);
        }
        else
            transform.rotation = Quaternion.Euler(90f, -90f, 0);
    }
}
