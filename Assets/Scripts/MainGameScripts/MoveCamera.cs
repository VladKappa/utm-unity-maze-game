using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UIElements;

public class MoveCamera : MonoBehaviour, ISaveable
{
    public Transform cameraPosition;

    public float startCoolDown;

    private KeyCode _mapKey;
    private bool canMove = false;
    private bool useFullMap = false;

    private Vector3 startPosition;

    void Start()
    {
        LoadJsonData(this);
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!useFullMap)
        {
            if (startCoolDown > 0)
                startCoolDown -= Time.deltaTime;
            else
            {
                canMove = true;
                transform.position = cameraPosition.position;
            }
        }

        if (Input.GetKeyDown(_mapKey) && startCoolDown <= 0)
        {
            if (!useFullMap)
            {
                useFullMap = true;
                canMove = false;
            }
            else
            {
                useFullMap = false;
                canMove = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (ShowFullMap())
        {
            ReturnCamera();
            //Debug.Log(string.Join(", ", transform.position.GetType().GetFields().Where(f => !f.IsLiteral).Select(f => f.GetValue(transform.position))));
        }
    }

    private void ReturnCamera()
    {
        if (startPosition != transform.position)
        {
            transform.position = startPosition;

            //Prin codul de mai jos camera se ridica animat pana la pozitia initiala (de verificat care variante este mai buna, animat sau static)

            /*transform.position = new Vector3((float)Math.Round(transform.position.x, 1), (float)Math.Round(transform.position.y, 1), (float)Math.Round(transform.position.z, 1));
            transform.position = new Vector3((startPosition.x > transform.position.x ? transform.position.x + 0.1f : startPosition.x < transform.position.x ? transform.position.x - 0.1f : transform.position.x),
                                             (startPosition.y > transform.position.y ? transform.position.y + 0.1f : startPosition.y < transform.position.y ? transform.position.y - 0.1f : transform.position.y),
                                             (startPosition.z > transform.position.z ? transform.position.z + 0.1f : startPosition.z < transform.position.z ? transform.position.z - 0.1f : transform.position.z));*/
        }
    }

    public bool ShowFullMap()
    {
        return useFullMap;
    }

    public bool GetMovePermision()
    {
        return canMove;
    }

    public void PopulateSaveData(SaveDataAbstract saveData)
    {
        throw new NotImplementedException();
    }

    FileManager _fileManager = new FileManager();
    private void LoadJsonData(MoveCamera moveCam)
    {
        _fileManager.LoadFile("PlayerSettings.dat", out string json);
        if (json != "")
        {
            SaveDataSettings sd = new SaveDataSettings();
            sd.LoadFromJson(json);

            moveCam.LoadFromSaveData(sd);
        }
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {
        SaveDataSettings saveSettings = saveData as SaveDataSettings;
        _mapKey = saveSettings._map;
    }
}
