using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCurrentCelula : MonoBehaviour
{
    bool _isPlayerOn;

    void OnCollisionEnter(Collision other)
    {
        _isPlayerOn = true;
    }

    void OnCollisionExit(Collision other)
    {
        _isPlayerOn = false;
    }

    public bool IsPlayerOn()
    {
        return _isPlayerOn;
    }
}
