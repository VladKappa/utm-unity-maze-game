using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCoin : MonoBehaviour
{

    public float rotateSpeed;

    void Update()
    {
        this.transform.Rotate(0, rotateSpeed, 0);
    }
}
