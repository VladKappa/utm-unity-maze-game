using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;


public static class Graph
{
	public static void AddNod(List<List<int>> list, int i, int j)
	{
		list[i].Add(j);
		list[j].Add(i);
	}

	public static void PrintMatriceAdiacenta(List<List<int>> adj, int marime, int[,] matrice_adj)
	{
		string matrice = "";
		for (int i = 0; i < marime; i++)
			{
				for(int j=0;j < adj[i].Count;j++)
					{
						matrice_adj[ i, adj[i][j] ] = 1;
						Debug.Log(string.Format("list {0}: {1}", i, adj[i][j]));
					}
			}

		for(int i =0; i<marime ; i++)
		{
			for(int j=0;j<marime;j++)
			{
				matrice += matrice_adj[i, j] + ", ";
			}
			matrice += "\n";
		}

		Debug.Log(matrice);
	}

    public static void PrintShortestDistance(List<List<int>> adj, int s, int dest, int v, List<CelulaLabirint> celule)
    {
        int[] pred = new int[v];

        BFS(adj, s, dest, v, pred);

        List<int> path = new List<int>();
        int crawl = dest;
        path.Add(crawl);

        while (pred[crawl] != -1)
        {
            path.Add(pred[crawl]);
            crawl = pred[crawl];
        }

        for (int i = path.Count - 1; i >= 0; i--)
        {
            celule[path[i]].SeteazaStare(StareCelula.DrumNecesar);
        }
    }

	private static bool BFS(List<List<int>> adj, int src, int dest, int v, int []pred)
	{

		List<int> queue = new List<int>();
	
		bool []visited = new bool[v];
	
		for (int i = 0; i < v; i++)
		{
			visited[i] = false;
			pred[i] = -1;
		}
	
		visited[src] = true;
		queue.Add(src);
	
		while (queue.Count != 0)
		{
			int u = queue[0];
			queue.RemoveAt(0);
			
			for (int i = 0;
							i < adj[u].Count; i++)
			{
				if (visited[adj[u][i]] == false)
				{
					visited[adj[u][i]] = true;
					pred[adj[u][i]] = u;
					queue.Add(adj[u][i]);
                    
                    // Daca nodul curent este destinatia - solutia a fost gasita
					if (adj[u][i] == dest)
						return true;
				}
			}
		}
		return false;
	}
	
}


public class GeneratorLabirint : MonoBehaviour, ISaveable
{
    [SerializeField] CelulaLabirint celulaPrefab;
    [SerializeField] GameObject finalCoinPrefab;
    [SerializeField] float hintCoolDown;

    Vector2Int marimeLabirint;

    public GameObject startCamera;
    MoveCamera _startCameraScript;

    Vector3 startPosition = new Vector3();
	
    List<List<int>> adj;
	List<CelulaLabirint> celule;

    KeyCode _hintKey;
    float _backUpHintCoolDown;
    bool _canUseHint = true;

    private void Start()
    {
        _backUpHintCoolDown = hintCoolDown;
        _startCameraScript = startCamera.GetComponent<MoveCamera>();

        LoadJsonData(this);

        int marime = marimeLabirint.x * marimeLabirint.y;
        adj = new List<List<int>>(marime);

        for (int i = 0; i < marime; i++)
        {
            adj.Add(new List<int>());
        }
        GenereazaLabirint(marimeLabirint);

        // graf.PrintMatriceAdiacenta(adj, marime, matrice_adj);
    }

    void GenereazaLabirint(Vector2Int marime)
    {
        celule = new List<CelulaLabirint>();

        // Creaza celulele in matricea MARIMExMARIME

        for (int x = 0; x < marime.x; x++) 
        {
            for (int y = 0; y < marime.y; y++) 
            {
                Vector3 pozitieCelula = new Vector3(x - (marime.x / 2f), 0 ,y - (marime.y / 2f));
                CelulaLabirint celulaNoua = Instantiate(celulaPrefab, pozitieCelula, Quaternion.identity, transform);
								celulaNoua.AjusteazaPereti();
                celulaNoua.SeteazaStare(StareCelula.Neparcurs);
                celule.Add(celulaNoua);
            }
        }

        List<CelulaLabirint> drumCurent = new List<CelulaLabirint>();
        List<CelulaLabirint> celuleParcurse = new List<CelulaLabirint>();

        drumCurent.Add(celule[0]);
        drumCurent[0].SeteazaStare(StareCelula.Curent);

        while(celuleParcurse.Count < celule.Count)
        {
            List<int> celuleVecinePosibile = new List<int>();
            List<int> directiiPosibile = new List<int>();

            int CelulaCurentaIndex = celule.IndexOf(drumCurent[drumCurent.Count - 1]);
            int CelulaCurentaX = CelulaCurentaIndex / marime.y;
            int CelulaCurentaY = CelulaCurentaIndex % marime.y;


            // O celula are urmatorii pereti setati:
            // 0 - +X
            // 1 - -X
            // 2 - +Z
            // 3 - -Z

            // Daca nu suntem in afara matricei pe axa X pozitiva
            if (CelulaCurentaX < marime.x - 1)
            {
                // Verificam celula din dreapta celulei curente
                if(
                // Verificam ca Celula aleasa sa nu fie deja parcursa
                !celuleParcurse.Contains(celule[CelulaCurentaIndex + marime.y]) &&
                // Verificam ca celula aleasa sa nu fie in drumul curent
                !drumCurent.Contains(celule[CelulaCurentaIndex + marime.y]))
                {
                    directiiPosibile.Add(1);
                    celuleVecinePosibile.Add(CelulaCurentaIndex + marime.y);
                }
            }

            // Daca nu suntem in afara matricei pe axa X negativa
            if (CelulaCurentaX > 0)
            {
                // Verificam celula din stanga celulei curente
                if(
                // Verificam ca Celula aleasa sa nu fie deja parcursa
                !celuleParcurse.Contains(celule[CelulaCurentaIndex - marime.y]) &&
                // Verificam ca celula aleasa sa nu fie in drumul curent
                !drumCurent.Contains(celule[CelulaCurentaIndex - marime.y]))
                {
                    directiiPosibile.Add(2);
                    celuleVecinePosibile.Add(CelulaCurentaIndex - marime.y);
                }
            }

            // Dca nu suntem in afara matricei pe axa Y pozitiva
            if (CelulaCurentaY < marime.y - 1)
            {
                // Verificam celula din fata celulei curente
                if(
                // Verificam ca Celula aleasa sa nu fie deja parcursa
                !celuleParcurse.Contains(celule[CelulaCurentaIndex + 1]) &&
                // Verificam ca celula aleasa sa nu fie in drumul curent
                !drumCurent.Contains(celule[CelulaCurentaIndex + 1]))
                {
                    directiiPosibile.Add(3);
                    celuleVecinePosibile.Add(CelulaCurentaIndex +1);
                }
            }


            // Dca nu suntem in afara matricei pe axa Y negativa
            if (CelulaCurentaY > 0)
            {
                // Verificam celula din spatele celulei curente
                if(
                // Verificam ca Celula aleasa sa nu fie deja parcursa
                !celuleParcurse.Contains(celule[CelulaCurentaIndex - 1]) &&
                // Verificam ca celula aleasa sa nu fie in drumul curent
                !drumCurent.Contains(celule[CelulaCurentaIndex - 1]))
                {
                    directiiPosibile.Add(4);
                    celuleVecinePosibile.Add(CelulaCurentaIndex - 1);
                }
            }


            if(directiiPosibile.Count > 0)
            {
                int directiaAleasa = UnityEngine.Random.Range(0, directiiPosibile.Count);
                CelulaLabirint celulaAleasa = celule[celuleVecinePosibile[directiaAleasa]];

                // O celula are urmatorii pereti setati:
                // 0 - +X 1
                // 1 - -X 2 
                // 2 - +Z 3
                // 3 - -Z 4

                switch(directiiPosibile[directiaAleasa])
                {
                    // Dreapta
                    case 1:
                        celulaAleasa.StergePerete(1);
                        drumCurent[drumCurent.Count -1].StergePerete(0);
                        break;
                    // Stanga
                    case 2:
                        celulaAleasa.StergePerete(0);
                        drumCurent[drumCurent.Count -1].StergePerete(1);
                        break;
                    // Sus
                    case 3:
                        celulaAleasa.StergePerete(3);
                        drumCurent[drumCurent.Count -1].StergePerete(2);
                        break;
                    // Jos
                    case 4:
                        celulaAleasa.StergePerete(2);
                        drumCurent[drumCurent.Count -1].StergePerete(3);
                        break;
                }
								Graph.AddNod(adj, celule.IndexOf(celulaAleasa), celule.IndexOf(drumCurent[drumCurent.Count -1]));

                drumCurent.Add(celulaAleasa);
                celulaAleasa.getPodea().material.color = Color.green;

                drumCurent[drumCurent.Count - 2].SeteazaStare(StareCelula.Curent);
            }
            else
            {
                celuleParcurse.Add(drumCurent[drumCurent.Count - 1]);

                if(drumCurent.Count - 2 >= 0)
                    drumCurent[drumCurent.Count - 2].getPodea().material.color = Color.green;
                drumCurent[drumCurent.Count - 1].SeteazaStare(StareCelula.Parcurs);
                drumCurent.RemoveAt(drumCurent.Count - 1);
            }
        }

        Vector3 endPosition = new Vector3();

        for (int i = 0; i < celule.Count; i++)
        {
            if(i == (marimeLabirint.x * marimeLabirint.y - marimeLabirint.y))
            {
                startPosition = new Vector3(celule[i].transform.position.x, 0, celule[i].transform.position.z);
                celule[i].SetTextures("start");
            }
            else if(i == (marimeLabirint.y - 1))
            {
                endPosition = new Vector3(celule[i].transform.position.x, celule[i].transform.position.y, celule[i].transform.position.z);
                celule[i].SetTextures("end");
            }
            else
            {
                celule[i].SetTextures();
            }
        }

        GameObject finalCoin = Instantiate(finalCoinPrefab, endPosition, Quaternion.identity, transform);
    }

    private void Update()
    {
        if (Input.GetKeyDown(_hintKey) && _canUseHint)
        {
            _canUseHint = false;

            Graph.PrintShortestDistance(adj, celule.IndexOf(celule.Where(c => c.IsPlayerOn()).First()), marimeLabirint.y - 1, marimeLabirint.x * marimeLabirint.y, celule);
        }

        if (_startCameraScript.ShowFullMap())
        {
            celule.Where(c => c.IsPlayerOn()).First().SeteazaStare(StareCelula.Curent);
        }
        else
        {
            celule.Where(c => c.IsPlayerOn()).First().getPodea().material.color = Color.white;
        }

        if (!_canUseHint)
        {
            if (hintCoolDown > 0)
                hintCoolDown -= Time.deltaTime;
            else
            {
                _canUseHint = true;
                hintCoolDown = _backUpHintCoolDown;
                foreach(CelulaLabirint celula in celule.Where(c => c.getStare() == StareCelula.DrumNecesar).ToList())
                {
                    celula.getPodea().material.color = Color.white;
                }
            }
        }
    }

    public Vector3 GetStartPosition()
    {
        return startPosition;
    }

    FileManager _fileManager = new FileManager();

    public void PopulateSaveData(SaveDataAbstract saveData)
    {

    }

    private void LoadJsonData(GeneratorLabirint genLab)
    {
        _fileManager.LoadFile("PlayerConfig.dat", out string jsonConfig);
        if (jsonConfig != "")
        {
            SaveDataConfig sd = new SaveDataConfig();
            sd.LoadFromJson(jsonConfig);

            genLab.LoadFromSaveData(sd);
        }

        _fileManager.LoadFile("PlayerSettings.dat", out string jsonSettings);
        if (jsonSettings != "")
        {
            SaveDataSettings sd = new SaveDataSettings();
            sd.LoadFromJson(jsonSettings);

            genLab.LoadFromSaveData(sd);
        }
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {
        if (saveData is SaveDataConfig)
        {
            SaveDataConfig saveConfig = saveData as SaveDataConfig;
            marimeLabirint = new Vector2Int(saveConfig._labWidth, saveConfig._labHeight);
        }
        else
        {
            SaveDataSettings saveSettings = saveData as SaveDataSettings;
            _hintKey = saveSettings._hint;
        }
    }
}
