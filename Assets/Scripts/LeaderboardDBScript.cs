using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine;

public class LeaderboardDBScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InitializeDB();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string iHateAdrian(string de_ce_nu_faci_timestamp_corect)
    {
        string[] splitted = de_ce_nu_faci_timestamp_corect.Split(':');
        if(splitted[0].Length == 1)
            splitted[0] = "0" + splitted[0];
        string[] minute = splitted[1].Split('.');
        if(minute[0].Length == 1)
            minute[0] = "0" + minute[0];
        return "00:"+splitted[0]+":"+minute[0]+"."+minute[1];
    }

    public DataTable grabData(int size=0)
    {
        IDbConnection dbConnection = getDBConnection();
        IDbCommand getData = dbConnection.CreateCommand();
        getData.CommandText = "select MazeSize,Nickname,Time from Leaderboard order by 3";

        if(size != 0)
            {
                var mazesize_param = getData.CreateParameter();
                mazesize_param.ParameterName = "@size";
                mazesize_param.Value = string.Format("{0}x{1}", size, size);
                getData.CommandText = "select MazeSize,Nickname,Time from Leaderboard where MazeSize=@size order by 3";
                getData.Parameters.Add(mazesize_param);
            }

        DataTable dt = new DataTable();

        IDataReader rd = getData.ExecuteReader();

        dt.Load(rd);

        return dt;
    }

    public void insertScore(int MazeSize, string name, string time)
    {
        InitializeDB();
        IDbConnection dbConnection = getDBConnection();
        IDbCommand insertData = dbConnection.CreateCommand();
        var mazesize_param = insertData.CreateParameter();

        mazesize_param.ParameterName = "@size";
        mazesize_param.Value = string.Format("{0}x{1}", MazeSize, MazeSize);

        var nickname_param = insertData.CreateParameter();
        nickname_param.ParameterName = "@name";
        nickname_param.Value = name;

        var time_param = insertData.CreateParameter();
        time_param.ParameterName = "@time";
        time_param.Value = iHateAdrian(time);

        



        insertData.CommandText = "insert into Leaderboard (MazeSize, Nickname, Time) VALUES (@size, @name, @time)";
        insertData.Parameters.Add(mazesize_param);
        insertData.Parameters.Add(nickname_param);
        insertData.Parameters.Add(time_param);

        insertData.ExecuteReader();

        dbConnection.Close();
    }


    private void InitializeDB()
    {
        IDbConnection dbConnection = getDBConnection();


        IDbCommand dbCommandCreateTable = dbConnection.CreateCommand();
        dbCommandCreateTable.CommandText = "CREATE TABLE IF NOT EXISTS Leaderboard (id INTEGER PRIMARY KEY AUTOINCREMENT, MazeSize TEXT, Nickname TEXT, Time TEXT)";
        dbCommandCreateTable.ExecuteReader();

        dbConnection.Close();
    }

    public IDbConnection getDBConnection()
    {
        string dbUri = "URI=file:" + Application.persistentDataPath + "/leaderboard.sqlite";
        IDbConnection dbConnection = new SqliteConnection(dbUri);
        dbConnection.Open();

        return dbConnection;
    }

}
