using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class KeyBindManagerScript: MonoBehaviour, ISaveable
{
    [SerializeField] GameObject _startMeniu;
    [SerializeField] GameObject _settingsMeniu;

    [SerializeField] TMP_Text _frontButtonText;
    [SerializeField] TMP_Text _backButtonText;
    [SerializeField] TMP_Text _leftButtonText;
    [SerializeField] TMP_Text _rightButtonText;
    [SerializeField] TMP_Text _hintButtonText;
    [SerializeField] TMP_Text _mapButtonText;

    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    private GameObject currentKey;

    private Color32 normal = new Color32(39, 171, 249, 255);
    private Color32 selected = new Color32(239, 116, 36, 255);

    public void GenerateNewSettings()
    {
        keys = new Dictionary<string, KeyCode>();

        keys.Add("FrontButton", KeyCode.None);
        keys.Add("BackButton", KeyCode.None);
        keys.Add("LeftButton", KeyCode.None);
        keys.Add("RightButton", KeyCode.None);
        keys.Add("HintButton", KeyCode.None);
        keys.Add("MapButton", KeyCode.None);

        LoadJsonData(this);
    }

    public void OnShow()
    {
        keys = new Dictionary<string, KeyCode>();

        keys.Add("FrontButton", KeyCode.None);
        keys.Add("BackButton", KeyCode.None);
        keys.Add("LeftButton", KeyCode.None);
        keys.Add("RightButton", KeyCode.None);
        keys.Add("HintButton", KeyCode.None);
        keys.Add("MapButton", KeyCode.None);

        LoadJsonData(this);

        _frontButtonText.text = keys["FrontButton"].ToString();
        _backButtonText.text = keys["BackButton"].ToString();
        _leftButtonText.text = keys["LeftButton"].ToString();
        _rightButtonText.text = keys["RightButton"].ToString();
        _hintButtonText.text = keys["HintButton"].ToString();
        _mapButtonText.text = keys["MapButton"].ToString();
    }

    void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;

            if (e.isMouse || e.isKey)
            {
                keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<TMP_Text>().text = e.keyCode.ToString();
                currentKey.GetComponent<Image>().color = normal;
                currentKey = null;
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        if(currentKey != null)
            currentKey.GetComponent<Image>().color = normal;

        currentKey = clicked;
        currentKey.GetComponent<Image>().color = selected;
    }

    public void BackButtonClick()
    {
        _startMeniu.SetActive(true);
        _settingsMeniu.SetActive(false);
    }

    public void SaveButtonClick()
    {
        SaveDataSettings sds = new SaveDataSettings();
        SaveJsonData(this, sds);

        BackButtonClick();
    }


    FileManager _fileManager = new FileManager();

    private void LoadJsonData(KeyBindManagerScript keyManager)
    {
        _fileManager.LoadFile("PlayerSettings.dat", out string json);

        SaveDataSettings sds = new SaveDataSettings();
        if (json != "")
        {
            sds.LoadFromJson(json);
        }
        else
        {
            _fileManager.WriteFile("PlayerSettings.dat", sds.ToJson());
        }
        keyManager.LoadFromSaveData(sds);
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {
        SaveDataSettings sds = saveData as SaveDataSettings;

        keys["FrontButton"] = sds._moveFront;
        keys["BackButton"] = sds._moveBack;
        keys["LeftButton"] = sds._moveLeft;
        keys["RightButton"] = sds._moveRight;
        keys["MapButton"] = sds._map;
        keys["HintButton"] = sds._hint;
    }

    private void SaveJsonData(KeyBindManagerScript keyManager, SaveDataSettings sds)
    {
        keyManager.PopulateSaveData(sds);

        _fileManager.WriteFile("PlayerSettings.dat", sds.ToJson());
    }

    public void PopulateSaveData(SaveDataAbstract saveData)
    {
        SaveDataSettings sds = saveData as SaveDataSettings;

        sds._moveFront = keys["FrontButton"];
        sds._moveBack = keys["BackButton"];
        sds._moveLeft = keys["LeftButton"];
        sds._moveRight = keys["RightButton"];
        sds._map = keys["MapButton"];
        sds._hint = keys["HintButton"];
    }
}
