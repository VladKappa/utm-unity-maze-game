using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayMeniuScript : MonoBehaviour, ISaveable
{
    [SerializeField] GameObject _startMeniu;
    [SerializeField] GameObject _playMeniu;

    public TMP_InputField _playerNameText;
    public TMP_Dropdown _labSizeText;
    public GameObject _nameUnderLinel;

    public void PlayButtonClick()
    {
        if (!string.IsNullOrEmpty(_playerNameText.text))
        {
            SaveJsonData(this);
            SceneManager.LoadScene("MainGameScene");
        }
        else
        {
            _nameUnderLinel.GetComponent<Image>().color = Color.red;
        }
    }

    public void BackButtonClick()
    {
        _playMeniu.SetActive(false);
        _startMeniu.SetActive(true);
    }

    static FileManager _fileManager = new FileManager();

    private static void SaveJsonData(PlayMeniuScript playMeniu)
    {
        SaveDataConfig sd = new SaveDataConfig();
        playMeniu.PopulateSaveData(sd);

        _fileManager.WriteFile("PlayerConfig.dat", sd.ToJson());
    }

    public void PopulateSaveData(SaveDataAbstract saveData)
    {
        SaveDataConfig saveConfig = saveData as SaveDataConfig;

        saveConfig._userName = _playerNameText.text;

        int dropDownIndex = _labSizeText.value;

        short[] sizes = _labSizeText.options[dropDownIndex].text.Split("x").Select(v => Convert.ToInt16(v)).ToArray();

        saveConfig._labWidth = sizes[0];
        saveConfig._labHeight = sizes[1];
        saveConfig._time = "";
    }

    private static void LoadJsonData(PlayMeniuScript playMeniu)
    {
        _fileManager.LoadFile("PlayerConfig.dat", out string json);
        if(json != "")
        {
            SaveDataConfig sd = new SaveDataConfig();
            sd.LoadFromJson(json);

            playMeniu.LoadFromSaveData(sd);
        }
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {
        SaveDataConfig saveConfig = saveData as SaveDataConfig; 
        _playerNameText.text = saveConfig._userName;
    }
}
