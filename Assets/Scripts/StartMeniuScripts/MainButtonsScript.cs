using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainButtonsScript : MonoBehaviour
{
    [SerializeField] GameObject _startMeniu;
    [SerializeField] GameObject _playMeniu;
    [SerializeField] GameObject _settingsMeniu;
    [SerializeField] GameObject _leaderboard;

    private void Awake()
    {
        _settingsMeniu.GetComponent<KeyBindManagerScript>().GenerateNewSettings();
    }

    public void PlayButtonClick()
    {
        _playMeniu.SetActive(true);
        _startMeniu.SetActive(false);
    }

    public void SettingsButtonClick()
    {
        _settingsMeniu.SetActive(true);
        _settingsMeniu.GetComponent<KeyBindManagerScript>().OnShow();
        _startMeniu.SetActive(false);
    }

    public void LeaderboardClick()
    {
        _leaderboard.SetActive(true);
        _startMeniu.SetActive(false);
    }

    public void ExitButtonClick()
    {
        Application.Quit();
    }
}
