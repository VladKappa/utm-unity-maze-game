using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Data;
using System;



public class LeaderboardScript : MonoBehaviour
{

    public LeaderboardDBScript DB;

    [SerializeField] GameObject _previousMenu;
    [SerializeField] GameObject LeaderboardRow;

    [SerializeField] GameObject sizefilter;

    GameObject lb;


    public void BackButtonClick()
    {
        GameObject.Find("Leaderboard").SetActive(false);
        _previousMenu.SetActive(true);
    }

    private void Awake()
    {
        TMPro.TMP_Dropdown m_sizefilter;
        m_sizefilter = sizefilter.GetComponent<TMPro.TMP_Dropdown>();
        m_sizefilter.onValueChanged.AddListener(delegate {
            sizefilterValueChanged(m_sizefilter);
        });



        string currentsize = m_sizefilter.captionText.text.Split('x')[0];

        

        DataTable dt = DB.grabData(int.Parse(currentsize));

        lb = GameObject.Find("LeaderboardRow");

        lb.SetActive(true);

        GameObject lbrow;

        if(dt.Rows.Count > 0)
            lb.SetActive(false);
        for(int i = 0; i< dt.Rows.Count;i++)
        {
            lbrow = Instantiate (LeaderboardRow, transform);
            lbrow.transform.SetParent(GameObject.Find("Panel").transform);
                for (int j = 0; j <dt.Columns.Count ; j++)
            {
                lbrow.transform.GetChild(j).GetComponent<TMPro.TextMeshProUGUI>().text = dt.Rows[i].ItemArray[j].ToString();
            }
        }

        
        
    }

    void sizefilterValueChanged(TMPro.TMP_Dropdown change)
    {
       RemoveAll();

        string currentsize = change.captionText.text.Split('x')[0];

        DataTable dt = null;
        try
        {
            dt = DB.grabData(int.Parse(currentsize));
        }
        catch(Exception)
        {
            return;
        }
        lb.SetActive(true);

        GameObject lbrow;

        if(dt.Rows.Count > 0)
            lb.SetActive(false);
        for(int i = 0; i< dt.Rows.Count;i++)
        {
            lbrow = Instantiate (LeaderboardRow, transform);
            lbrow.transform.SetParent(GameObject.Find("Panel").transform);
                for (int j = 0; j <dt.Columns.Count ; j++)
            {
                lbrow.transform.GetChild(j).GetComponent<TMPro.TextMeshProUGUI>().text = dt.Rows[i].ItemArray[j].ToString();
            }
        }

    }

    public void RemoveAll()
    {
        GameObject panel = GameObject.Find("Panel");

        foreach (Transform child in panel.transform) {
            if(child.gameObject != lb)
            GameObject.Destroy(child.gameObject);
        }
    }

}
