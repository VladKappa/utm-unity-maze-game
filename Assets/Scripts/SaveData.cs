using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;

public class SaveDataSettings : SaveDataAbstract
{
    public KeyCode _moveFront;
    public KeyCode _moveBack;
    public KeyCode _moveLeft;
    public KeyCode _moveRight;

    public KeyCode _hint;
    public KeyCode _map;

    public SaveDataSettings()
    {
        _moveFront = KeyCode.W;
        _moveBack = KeyCode.S;
        _moveLeft = KeyCode.A;
        _moveRight = KeyCode.D;

        _hint = KeyCode.Q;
        _map = KeyCode.M;
    }
}

public class SaveDataConfig : SaveDataAbstract
{
    public string _userName;
    public string _time;

    public int _labWidth;
    public int _labHeight;
}

[System.Serializable]
public abstract class SaveDataAbstract
{
    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
    }
}

public interface ISaveable
{
    void PopulateSaveData(SaveDataAbstract saveData);
    void LoadFromSaveData(SaveDataAbstract saveData);
}
