using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Threading;
using System;
using UnityEngine.UI;

public class PlayerScoreActivities : MonoBehaviour, ISaveable
{
    public LeaderboardDBScript DB;

    [SerializeField] TMP_Text _playerNick;
    [SerializeField] TMP_Text _playerLabSize;
    [SerializeField] TMP_Text _playerTime;

    [SerializeField] GameObject _leaderboard;
    [SerializeField] GameObject _playerScore;


    void Awake()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Start()
    {
        LoadJsonData(this);
    }

    public void ShowLeaderboard()
    {
        _leaderboard.SetActive(true);
        _playerScore.SetActive(false);
    }

    public void ExitButtonClick()
    {
        SceneManager.LoadScene("StartScene");
    }

    FileManager _fileManager = new FileManager();
    private void LoadJsonData(PlayerScoreActivities activities)
    {
        _fileManager.LoadFile("PlayerConfig.dat", out string json);
        if (json != "")
        {
            SaveDataConfig sd = new SaveDataConfig();
            sd.LoadFromJson(json);

            activities.LoadFromSaveData(sd);
        }
    }

    public void LoadFromSaveData(SaveDataAbstract saveData)
    {
        SaveDataConfig saveConfig = saveData as SaveDataConfig;

        _playerNick.text = saveConfig._userName;
        _playerLabSize.text = $"{saveConfig._labWidth}x{saveConfig._labHeight}";
        _playerTime.text = saveConfig._time;

        DB.insertScore(saveConfig._labWidth, _playerNick.text, _playerTime.text);
    }

    public void PopulateSaveData(SaveDataAbstract saveData)
    {

    }
}
