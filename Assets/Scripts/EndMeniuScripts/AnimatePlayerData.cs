using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AnimatePlayerData : MonoBehaviour
{
    [SerializeField] TMP_Text _labelNick;
    [SerializeField] TMP_Text _playerNick;
    [SerializeField] TMP_Text _labelLabSize;
    [SerializeField] TMP_Text _playerLabSize;
    [SerializeField] TMP_Text _labelTime;
    [SerializeField] TMP_Text _playerTime;
    [SerializeField] Button _leaderBoardButton;
    [SerializeField] Button _exitButton;

    Vector3 _initLabelNick;
    Vector3 _initSizeNick;
    Vector3 _initLabelLabSize;
    Vector3 _initSizeLabSize;
    Vector3 _initLabelTime;
    Vector3 _initSizeTime;

    void Start()
    {
        _initLabelNick = new Vector3(_labelNick.transform.localScale.x, _labelNick.transform.localScale.y, _labelNick.transform.localScale.z);
        _initSizeNick = new Vector3(_playerNick.transform.localScale.x, _playerNick.transform.localScale.y, _playerNick.transform.localScale.z);
        _initLabelLabSize = new Vector3(_labelLabSize.transform.localScale.x, _labelLabSize.transform.localScale.y, _labelLabSize.transform.localScale.z);
        _initSizeLabSize = new Vector3(_playerLabSize.transform.localScale.x, _playerLabSize.transform.localScale.y, _playerLabSize.transform.localScale.z);
        _initLabelTime = new Vector3(_labelTime.transform.localScale.x, _labelLabSize.transform.localScale.y, _labelLabSize.transform.localScale.z);
        _initSizeTime = new Vector3(_playerTime.transform.localScale.x, _playerTime.transform.localScale.y, _playerTime.transform.localScale.z);

        _labelNick.transform.localScale = new Vector3(_initLabelNick.x * 1.5f, _initLabelNick.y * 1.5f, _initLabelNick.z);
        _playerNick.transform.localScale = new Vector3(_initSizeNick.x * 1.5f, _initSizeNick.y * 1.5f, _initSizeNick.z);
        _labelLabSize.transform.localScale = new Vector3(_initLabelLabSize.x * 1.5f, _initLabelLabSize.y * 1.5f, _initLabelLabSize.z);
        _playerLabSize.transform.localScale = new Vector3(_initSizeLabSize.x * 1.5f, _initSizeLabSize.y * 1.5f, _initSizeLabSize.z);
        _labelTime.transform.localScale = new Vector3(_initLabelTime.x * 1.5f, _initLabelTime.y * 1.5f, _initLabelTime.z);
        _playerTime.transform.localScale = new Vector3(_initSizeTime.x * 1.5f, _initSizeTime.y * 1.5f, _initSizeTime.z);
    }

    void FixedUpdate()
    {
        if (!_playerNick.transform.localScale.Equals(_initSizeNick) && !_labelNick.transform.localScale.Equals(_initLabelNick))
        {
            if (_playerNick.transform.localScale.x != _initSizeNick.x)
                _playerNick.transform.localScale = new Vector3(_playerNick.transform.localScale.x - 0.01f, _playerNick.transform.localScale.y, _playerNick.transform.localScale.z);
            if (_playerNick.transform.localScale.y != _initSizeNick.y)
                _playerNick.transform.localScale = new Vector3(_playerNick.transform.localScale.x, _playerNick.transform.localScale.y - 0.01f, _playerNick.transform.localScale.z);
            _playerNick.transform.localScale = new Vector3((float)Math.Round(_playerNick.transform.localScale.x, 2),
                                                           (float)Math.Round(_playerNick.transform.localScale.y, 2),
                                                           _playerNick.transform.localScale.z);

            if (_labelNick.transform.localScale.x != _initLabelNick.x)
                _labelNick.transform.localScale = new Vector3(_labelNick.transform.localScale.x - 0.01f, _labelNick.transform.localScale.y, _labelNick.transform.localScale.z);
            if (_labelNick.transform.localScale.y != _initLabelNick.y)
                _labelNick.transform.localScale = new Vector3(_labelNick.transform.localScale.x, _labelNick.transform.localScale.y - 0.01f, _labelNick.transform.localScale.z);
            _labelNick.transform.localScale = new Vector3((float)Math.Round(_labelNick.transform.localScale.x, 2),
                                                          (float)Math.Round(_labelNick.transform.localScale.y, 2),
                                                          _labelNick.transform.localScale.z);
        }
        else if (!_playerLabSize.transform.localScale.Equals(_initSizeLabSize) && !_labelLabSize.transform.localScale.Equals(_initLabelLabSize))
        {
            _labelLabSize.gameObject.SetActive(true);
            _playerLabSize.gameObject.SetActive(true);

            if (_playerLabSize.transform.localScale.x != _initSizeLabSize.x)
                _playerLabSize.transform.localScale = new Vector3(_playerLabSize.transform.localScale.x - 0.01f, _playerLabSize.transform.localScale.y, _playerLabSize.transform.localScale.z);
            if (_playerLabSize.transform.localScale.y != _initSizeLabSize.y)
                _playerLabSize.transform.localScale = new Vector3(_playerLabSize.transform.localScale.x, _playerLabSize.transform.localScale.y - 0.01f, _playerLabSize.transform.localScale.z);
            _playerLabSize.transform.localScale = new Vector3((float)Math.Round(_playerLabSize.transform.localScale.x, 2),
                                                           (float)Math.Round(_playerLabSize.transform.localScale.y, 2),
                                                           _playerLabSize.transform.localScale.z);

            if (_labelLabSize.transform.localScale.x != _initLabelLabSize.x)
                _labelLabSize.transform.localScale = new Vector3(_labelLabSize.transform.localScale.x - 0.01f, _labelLabSize.transform.localScale.y, _labelLabSize.transform.localScale.z);
            if (_labelLabSize.transform.localScale.y != _initLabelLabSize.y)
                _labelLabSize.transform.localScale = new Vector3(_labelLabSize.transform.localScale.x, _labelLabSize.transform.localScale.y - 0.01f, _labelLabSize.transform.localScale.z);
            _labelLabSize.transform.localScale = new Vector3((float)Math.Round(_labelLabSize.transform.localScale.x, 2),
                                                          (float)Math.Round(_labelLabSize.transform.localScale.y, 2),
                                                          _labelLabSize.transform.localScale.z);
        }
        else if (!_playerTime.transform.localScale.Equals(_initSizeTime) && !_labelTime.transform.localScale.Equals(_initLabelTime))
        {
            _labelTime.gameObject.SetActive(true);
            _playerTime.gameObject.SetActive(true);

            if (_playerTime.transform.localScale.x != _initSizeTime.x)
                _playerTime.transform.localScale = new Vector3(_playerTime.transform.localScale.x - 0.01f, _playerTime.transform.localScale.y, _playerTime.transform.localScale.z);
            if (_playerTime.transform.localScale.y != _initSizeTime.y)
                _playerTime.transform.localScale = new Vector3(_playerTime.transform.localScale.x, _playerTime.transform.localScale.y - 0.01f, _playerTime.transform.localScale.z);
            _playerTime.transform.localScale = new Vector3((float)Math.Round(_playerTime.transform.localScale.x, 2),
                                                           (float)Math.Round(_playerTime.transform.localScale.y, 2),
                                                           _playerTime.transform.localScale.z);

            if (_labelTime.transform.localScale.x != _initLabelTime.x)
                _labelTime.transform.localScale = new Vector3(_labelTime.transform.localScale.x - 0.01f, _labelTime.transform.localScale.y, _labelTime.transform.localScale.z);
            if (_labelTime.transform.localScale.y != _initLabelTime.y)
                _labelTime.transform.localScale = new Vector3(_labelTime.transform.localScale.x, _labelTime.transform.localScale.y - 0.01f, _labelTime.transform.localScale.z);
            _labelTime.transform.localScale = new Vector3((float)Math.Round(_labelTime.transform.localScale.x, 2),
                                                          (float)Math.Round(_labelTime.transform.localScale.y, 2),
                                                          _labelTime.transform.localScale.z);
        }
        else
        {
            _leaderBoardButton.gameObject.SetActive(true);
            _exitButton.gameObject.SetActive(true);

            this.GetComponent<AnimatePlayerData>().enabled = false;
        }
    }
}
